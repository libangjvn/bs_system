package com.bssystem.bs_system.service;

import com.bssystem.bs_system.domain.entity.User;
import com.bssystem.bs_system.domain.vo.ResponseResult;

public interface LoginService {
    ResponseResult login(User user);

    ResponseResult logout();
}
