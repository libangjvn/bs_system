package com.bssystem.bs_system;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.bssystem.bs_system.mapper")
public class BsSystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(BsSystemApplication.class, args);
	}

}
