package com.bssystem.bs_system.exception;

import com.bssystem.bs_system.enums.AppHttpCodeEnum;

/**
 * 将抛出的异常进行处理
 */

public class SystemException extends RuntimeException{
    private int code;

    private String msg;

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    public SystemException(AppHttpCodeEnum httpCodeEnum) {
        super(httpCodeEnum.getMsg());
        this.code = httpCodeEnum.getCode();
        this.msg = httpCodeEnum.getMsg();
    }
}
