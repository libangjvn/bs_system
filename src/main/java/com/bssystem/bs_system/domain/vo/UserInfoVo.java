package com.bssystem.bs_system.domain.vo;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class UserInfoVo {
    /**
     * 主键
     */
    private Long id;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 邮箱
     */
    private  String email;

    /**
     * 手机号
     */
    private Long mobile;

    /**
     * 签名（图片）
     */
    private String signatrue;

    /**
     * 状态
     */
    private String status;

    /**
     * 创建时间
     */

    /**
     * 修改时间
     */



}