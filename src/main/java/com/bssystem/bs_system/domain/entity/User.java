package com.bssystem.bs_system.domain.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 用户表(UserTable)表实体类
 *
 * @author makejava
 * @since 2022-10-11 10:20:04
 */
@SuppressWarnings("serial")
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("user_table")
public class User {
    //主键
    private Long id;
    //用户名
    private String name;
    //用户密码
    private String password;
    //邮箱
    private String email;
    //手机号
    private Long mobile;
    //昵称
    private String nickname;
    //头像
    private String avatar;
    //签名（存图片）
    private String signatrue;
    //状态
    private String status;
    //创建时间
    private Date createTime;
    //修改时间
    private Date updateTime;

}



