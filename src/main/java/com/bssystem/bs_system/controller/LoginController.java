package com.bssystem.bs_system.controller;

import com.bssystem.bs_system.domain.entity.User;
import com.bssystem.bs_system.domain.vo.ResponseResult;
import com.bssystem.bs_system.enums.AppHttpCodeEnum;
import com.bssystem.bs_system.exception.SystemException;
import com.bssystem.bs_system.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class LoginController {
    @Autowired
    private LoginService loginService;

    @PostMapping("/login")
    public ResponseResult login(@RequestBody User user){
        if(!StringUtils.hasText(user.getName())){
            //提示 必须要传用户名
            throw new SystemException(AppHttpCodeEnum.REQUIRE_USERNAME);
        }

        return loginService.login(user);
    }


    @PostMapping("/logout")
    public ResponseResult logout(){
        return loginService.logout();
    }

}
