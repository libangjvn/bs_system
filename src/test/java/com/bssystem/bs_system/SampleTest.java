package com.bssystem.bs_system;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.bssystem.bs_system.domain.entity.User;
import com.bssystem.bs_system.mapper.UserMapper;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;


@SpringBootTest
public class SampleTest {

    @Autowired
    private UserMapper userMapper;

    @Test
    public void testSelect() {
        System.out.println(("----- selectAll method test ------"));
        //参数是一个Wrapper，条件结构器，这里先不用 填null
        //查询所有的用户
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(User::getName,"sg");
        User user = userMapper.selectOne(queryWrapper);

        System.out.println(user);
    }

}
